//
//  FakeLibaryEndpoint.swift
//  HenrisLibTests
//
//  Created by Jérôme Duquennoy on 16/07/2018.
//  Copyright © 2018 jduquennoy. All rights reserved.
//

import Foundation
@testable import HenrisLib

class FakeLibaryEndpoint : ResourcesLoader {
  var offersToReturn: [Offer] = []
  var booksToReturn: [Book] = []
  
  func get(_ resource: ApplicableOffers, callback: @escaping (TransfertResult<OffersResponse>) -> Void) {
    let response = OffersResponse(offers: offersToReturn)
    callback(.success(response))
  }
  
  func list<Resource>(_ resource: Resource, callback: @escaping (TransfertResult<[Resource.ResourceType]>) -> Void) where Resource : ListableResource {
    let result: [Resource.ResourceType]
    
    if resource as? AllBooks != nil {
      result = booksToReturn as! [Resource.ResourceType]
      callback(TransfertResult<[Resource.ResourceType]>.success(result))
    }
  }
  
  func get<Resource>(_ resource: Resource, callback: @escaping (TransfertResult<Resource.ResourceType>) -> Void) where Resource : TransferableResource {
  }
  
  func getObject<Resource>(_ resource: Resource, callback: @escaping (TransfertResult<Resource.ResourceType>) -> Void) where Resource : TransferableObject {
  }
  
}
