//
//  Transferable.swift
//  Reporter
//
//  Created by Jérôme Duquennoy on 22/03/2018.
//  Copyright © 2018 duquennoy. All rights reserved.
//

import UIKit

// TransferableResource and TransferableObject cannot be merged because of
// limitations of the generics model of swift, that causes encoders to not
// be able to work on objects defined as Codable, but only on concrete types
// implementing Codable.

protocol RepresentableByData {
  init?(data: Data)
}

protocol Transferable {
  /// The path to use to access the resource.
  /// This path will be appeneded to a base url handled by the ResourcesLoader.
  var getPath: String { get }
}

/// A binary resource that can be transfered from or to a remote source.
protocol TransferableResource: Transferable {
  associatedtype ResourceType: RepresentableByData
}

/// A codable object that can be transfered from or to a remote server
protocol TransferableObject: Transferable {
  associatedtype ResourceType: Decodable
}

/// A decodable object that can be listed from a remote source
protocol ListableResource {
  associatedtype ResourceType: Decodable
  
  // The path to use to list the resources.
  /// This path will be appeneded to a base url handled by the ResourcesLoader.
  var listPath: String { get }
}

extension UIImage: RepresentableByData {
}
