//
//  Book.swift
//  HenrysLib
//
//  Created by Jérôme Duquennoy on 11/07/2018.
//  Copyright © 2018 Jérôme Duquennoy. All rights reserved.
//

import UIKit

struct Book: Decodable, Equatable, Hashable {
  let cover: URL
  let isbn: UUID
  let price: Float
  let synopsis: [String]
  let title: String

  var hashValue: Int { return isbn.hashValue }
  static func ==(lhs: Book, rhs: Book) -> Bool {
    return lhs.isbn == rhs.isbn
  }
}

struct AllBooks: ListableResource {
  typealias ResourceType = Book
  var listPath: String { return "/books" }
}

struct BookCover: TransferableResource {
  typealias ResourceType = UIImage
  
  let book: Book
  
  var getPath: String {
    return book.cover.absoluteString
  }
}
