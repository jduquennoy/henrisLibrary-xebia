//
//  BooksSetConsumer.swift
//  HenrisLib
//
//  Created by Jérôme Duquennoy on 16/07/2018.
//  Copyright © 2018 jduquennoy. All rights reserved.
//

import Foundation

/// This protocol should be implemented by view controllers that needs to receive a list of
/// books when navigated to.
protocol BooksSetConsumer {
  func load(books: [Book], with resourcesLoader: ResourcesLoader)
}
