//
//  BooksListViewControllerTests.swift
//  HenrisLibTests
//
//  Created by Jérôme Duquennoy on 16/07/2018.
//  Copyright © 2018 jduquennoy. All rights reserved.
//

import XCTest
@testable import HenrisLib

class BooksListViewControllerTests: XCTestCase {
    
  func testControllerLoadsBooksWhenBecomingVisible() {
    let fakeEndpoint = FakeLibaryEndpoint()
    let controller = BooksListViewController()
    controller.libraryEndpoint = fakeEndpoint
    
    fakeEndpoint.booksToReturn = [
      Book(cover: URL(string: "http://url1")!, isbn: UUID(), price: 10.0, synopsis: [], title: "title 1" ),
      Book(cover: URL(string: "http://url2")!, isbn: UUID(), price: 11.0, synopsis: [], title: "title 2" ),
      Book(cover: URL(string: "http://url3")!, isbn: UUID(), price: 12.0, synopsis: [], title: "title 3" ),
    ]
    
    // Execute
    controller.viewWillAppear(false)
    
    XCTAssertEqual(controller.books?.count, fakeEndpoint.booksToReturn.count)
  }
  
}
