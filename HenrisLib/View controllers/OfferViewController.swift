//
//  OfferViewController.swift
//  HenrisLib
//
//  Created by Jérôme Duquennoy on 16/07/2018.
//  Copyright © 2018 jduquennoy. All rights reserved.
//

import UIKit

class OfferViewController: UIViewController {
  @IBOutlet var descriptionLabel: UILabel?
  @IBOutlet var priceLabel: UILabel?
  @IBOutlet var rebateDescriptionLabel: UILabel?

  private func offerDidLoad(_ result: TransfertResult<OffersResponse>, for books: [Book]) {
    switch result {
    case .error(let error):
      NSLog("Failed to load offer: \(error)")
      DispatchQueue.main.async {
        self.priceLabel?.text = "⚠️"
      }
    case .success(let offersResponse):
      let originalPrice = books.reduce(0) { sum, book in return sum + book.price }
      let bestOffer = offersResponse.offers.bestOffer(for: originalPrice)
      DispatchQueue.main.async {
        self.priceLabel?.text = String(format: "%.2f €", bestOffer?.apply(to: originalPrice) ?? originalPrice)
        self.rebateDescriptionLabel?.text = bestOffer?.description ?? "No rebate for you"
      }
    }
  }
}

extension OfferViewController: BooksSetConsumer {
  func load(books: [Book], with resourcesLoader: ResourcesLoader) {
    guard books.count > 0 else {
      descriptionLabel?.text = "You should select books before trying to buy them."
      return
    }
    
    self.loadViewIfNeeded()

    switch(books.count) {
    case 1:
      descriptionLabel?.text = "So you want to buy a single book.\nHere is our best offer:"
    default:
      descriptionLabel?.text = "So you want to buy \(books.count) books.\nHere is our best offer:"
    }
    priceLabel?.text = "..."

    let offerRequest = ApplicableOffers(booksReferences: books.map { $0.isbn })
    resourcesLoader.getObject(offerRequest) { result in
      self.offerDidLoad(result, for: books)
    }
  }
}
