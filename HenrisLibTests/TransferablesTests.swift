//
//  TransferablesTests.swift
//  HenrisLibTests
//
//  Created by Jérôme Duquennoy on 16/07/2018.
//  Copyright © 2018 jduquennoy. All rights reserved.
//

import XCTest
@testable import HenrisLib

class TransferablesTests: XCTestCase {
  
  func testApplicableOffersGetUrlContainsLowercaseUUIDs() {
    let uuid1 = UUID()
    let uuid2 = UUID()
    let applicableOffers = ApplicableOffers(booksReferences: [uuid1, uuid2])
    
    // Execute
    let getUrl = applicableOffers.getPath
    
    XCTAssert(getUrl.contains(uuid1.uuidString.lowercased()))
    XCTAssert(getUrl.contains(uuid2.uuidString.lowercased()))
  }
  
}
