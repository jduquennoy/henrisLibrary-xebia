//
//  ResourcesLoader.swift
//  HenysLib
//
//  Created by Jérôme Duquennoy on 11/07/2018.
//  Copyright © 2018 duquennoy. All rights reserved.
//

import Foundation

enum TransfertResult<T> {
  case success(T)
  case error(Error)
}

typealias TransfertCallback<T> = (TransfertResult<T>) -> Void
typealias ListCallback<T> = (TransfertResult<[T]>) -> Void

/// A ResourceLoader can get, or delete resources conformting to the TransferableResource protocol.
/// It can also list resources conforming to the ListableResource protocol.
protocol ResourcesLoader: class {
  func list<Resource: ListableResource>(_ resource: Resource, callback: @escaping ListCallback<Resource.ResourceType>)
  func get<Resource: TransferableResource>(_ resource: Resource, callback: @escaping TransfertCallback<Resource.ResourceType>)
  func getObject<Resource: TransferableObject>(_ resource: Resource, callback: @escaping TransfertCallback<Resource.ResourceType>)
}

/// The RESTResourcesLoader is an implementation of ResourcesLoader for REST-ready services
class RESTResourcesLoader {
  private let baseUrl: URL

  public init(baseUrl: URL) {
    self.baseUrl = baseUrl
  }

  private func getUrl<T: Transferable>(for resource: T) -> URL? {
    let path = resource.getPath
    if !path.hasPrefix(self.baseUrl.absoluteString) {
      return self.baseUrl.appendingPathComponent(path)
    } else  {
      return URL(string: path)
    }
  }
  
  private func listUrl<T: ListableResource>(for resource: T) -> URL {
    return self.baseUrl.appendingPathComponent(resource.listPath)
  }
  
  private func decode<T: Decodable>(data: Data) throws -> T {
    let decodedData: T
    let decoder = JSONDecoder()
    decodedData = try decoder.decode(T.self, from: data)

    return decodedData
  }

  private func decode<T: RepresentableByData>(data: Data) throws -> T {
    guard let decodedObject = T(data: data) else {
      throw NSError.loaderError(with: "Could not decode object")
    }
    return decodedObject
  }
}

extension RESTResourcesLoader: ResourcesLoader {
  
  func list<Resource: ListableResource>(_ resource: Resource, callback: @escaping ListCallback<Resource.ResourceType>) {
    let url = self.listUrl(for: resource)
    
    let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
      guard error == nil else {
        callback(.error(error!))
        return
      }
      guard let data = data else {
        let error = NSError.httpError(response: response, message: "No data received")
        callback(.error(error))
        return
      }

      do {
        let decodedResource:[Resource.ResourceType] = try self.decode(data: data)
        callback(TransfertResult.success(decodedResource))
      } catch {
        callback(TransfertResult.error(error))
      }
    }
    task.resume()
  }
  
  func get<Resource: TransferableResource>(_ resource: Resource, callback: @escaping TransfertCallback<Resource.ResourceType>) {
    guard let url = self.getUrl(for: resource) else {
      let result = TransfertResult<Resource.ResourceType>.error(NSError.loaderError(with:  "Invalid get URL for resource \(resource)"))
      callback(result)
      return
    }
    let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
      guard error == nil else {
        callback(.error(error!))
        return
      }
      guard let data = data else {
        let error = NSError.httpError(response: response, message: "No data received")
        callback(.error(error))
        return
      }
      
      do {
        let decodedResource:Resource.ResourceType = try self.decode(data: data)
        callback(TransfertResult.success(decodedResource))
      } catch {
        callback(TransfertResult.error(error))
      }
    }
    task.resume()
  }

  func getObject<Resource: TransferableObject>(_ resource: Resource, callback: @escaping TransfertCallback<Resource.ResourceType>) {
    guard let url = self.getUrl(for: resource) else {
      let result = TransfertResult<Resource.ResourceType>.error(NSError.loaderError(with:  "Invalid get URL for resource \(resource)"))
      callback(result)
      return
    }
    let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
      guard error == nil else {
        callback(.error(error!))
        return
      }
      guard let data = data else {
        let error = NSError.httpError(response: response, message: "No data received")
        callback(.error(error))
        return
      }
      
      do {
        let decodedResource:Resource.ResourceType = try self.decode(data: data)
        callback(TransfertResult.success(decodedResource))
      } catch {
        callback(TransfertResult.error(error))
      }
    }
    task.resume()
  }}

private extension String {
  func utf8Data() throws -> Data {
    if let data = self.data(using: .utf8) {
      return data
    } else {
      throw NSError(domain: String(describing: type(of: self)), code: 0, userInfo: [NSLocalizedDescriptionKey: "String cannot be encoded as utf-8: \(self)"])
    }
  }
}

private extension NSError {
  class func loaderError(with message: String) -> NSError {
    var userInfo = [String: String]()
    userInfo[NSDebugDescriptionErrorKey] = message
    return NSError(domain: "Loader", code: 0, userInfo: userInfo)
  }
  
  class func httpError(response: URLResponse?, message: String?) -> NSError {
    var userInfo = [String: String]()
    var statusCode = 0
    
    if let message = message {
      userInfo[NSDebugDescriptionErrorKey] = message
    }
    if let response = response as? HTTPURLResponse {
      userInfo[NSLocalizedDescriptionKey] = "Serveur returned unexpected HTTP code \(response.statusCode)"
      statusCode = response.statusCode
    } else if let response = response {
      userInfo[NSLocalizedDescriptionKey] = "Serveur returned unexpected response: \(response)"
    } else {
      userInfo[NSLocalizedDescriptionKey] = "Server returned no answer"
    }
    
    return NSError(domain: "HTTP", code: statusCode, userInfo: userInfo)
  }
}
