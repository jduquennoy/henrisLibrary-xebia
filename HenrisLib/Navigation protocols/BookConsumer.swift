//
//  BookConsumer.swift
//  HenrisLib
//
//  Created by Jérôme Duquennoy on 13/07/2018.
//  Copyright © 2018 jduquennoy. All rights reserved.
//

import Foundation

/// This protocol should be implemented by view controllers that needs to receive a book
/// when navigated to.
protocol BookConsumer {
  func load(book: Book?, with resourcesLoader: ResourcesLoader)
}
