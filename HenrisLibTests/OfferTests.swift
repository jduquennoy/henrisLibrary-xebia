//
//  OfferTests.swift
//  HenrisLibTests
//
//  Created by Jérôme Duquennoy on 16/07/2018.
//  Copyright © 2018 jduquennoy. All rights reserved.
//

import XCTest
@testable import HenrisLib

class OfferTests: XCTestCase {
    
  func testPercentageOfferIsAppliedCorrectly() {
    let initialPrice = Float(123.45)
    let percentageOffer = Offer.percentage(value: 10.0)
    
    // Execute
    let finalPrice = percentageOffer.apply(to: initialPrice)
    
    XCTAssertEqual(finalPrice, initialPrice * 0.9)
  }
  
  func testMinusOfferIsAppliedCorrectly() {
    let initialPrice = Float(123.45)
    let rebate = Float(10.40)
    let percentageOffer = Offer.minus(value: rebate)
    
    // Execute
    let finalPrice = percentageOffer.apply(to: initialPrice)
    
    XCTAssertEqual(finalPrice, initialPrice - rebate)
  }

  func testSliceOfferIsAppliedCorrectly() {
    let initialPrice = Float(43.5)
    let rebate = Float(4)
    let percentageOffer = Offer.slice(value: rebate, slice: 10)
    
    // Execute
    let finalPrice = percentageOffer.apply(to: initialPrice)
    
    XCTAssertEqual(finalPrice, initialPrice - rebate * 4)
  }

  func testBestOfferForPriceReturnsCorrectOffer() {
    let offers = [
      Offer.minus(value: 5.0),
      Offer.percentage(value: 10.0),
      Offer.slice(value: 15.0, slice: 100.0)
    ]
    
    // Execute
    let bestOffer1 = offers.bestOffer(for: 10)
    let bestOffer2 = offers.bestOffer(for: 70)
    let bestOffer3 = offers.bestOffer(for: 120)
    
    XCTAssertEqual(bestOffer1, offers[0])
    XCTAssertEqual(bestOffer2, offers[1])
    XCTAssertEqual(bestOffer3, offers[2])
  }
}
