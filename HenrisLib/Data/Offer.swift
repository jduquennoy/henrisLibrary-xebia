//
//  Offer.swift
//  HenrysLib
//
//  Created by Jérôme Duquennoy on 11/07/2018.
//  Copyright © 2018 Jérôme Duquennoy. All rights reserved.
//

import Foundation

enum Offer: Decodable, Equatable {
  private enum CodingKeys: CodingKey {
    case type
    case value
    case sliceValue
  }
  
  case percentage(value: Float)
  case minus(value: Float)
  case slice(value: Float, slice: Float)

  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    let type = try container.decode(String.self, forKey: .type)
    let value = try container.decode(Float.self, forKey: .value)
    
    switch type {
    case "percentage":
      self = .percentage(value: value)
    case "minus":
      self = .minus(value: value)
    case "slice":
      let slice = try container.decode(Float.self, forKey: .sliceValue)
      self = .slice(value: value, slice: slice)
    default:
      throw DecodingError.dataCorruptedError(forKey: CodingKeys.type, in: container, debugDescription: "Unknown type '\(type)'")
    }
  }
  
  func apply(to amount: Float) -> Float {
    switch self {
    case .minus(let value):
      return amount - value
    case .percentage(let value):
      return amount * (1 - value/100)
    case .slice(let value, let slice):
      return amount - (value * floor(amount / slice))
    }
  }
  
  var description: String {
    switch self {
    case .minus(let value):
      return "You save \(value) €"
    case .percentage(let value):
      return "You save \(value) %"
    case .slice(let value, let slice):
      return "You save \(value) € every \(slice) €"
    }
  }

  static func ==(lhs: Offer, rhs: Offer) -> Bool {
    switch(lhs, rhs) {
    case let (.minus(lvalue), .minus(rvalue)):
      return lvalue == rvalue
    case let (.percentage(lvalue), .percentage(rvalue)):
      return lvalue == rvalue
    case let (.slice(lvalue, lslice), .slice(rvalue, rslice)):
      return lvalue == rvalue && lslice == rslice
    default:
      return false
    }
  }
}

extension Array where Array.Element == Offer {
  func bestOffer(for price: Float) -> Offer? {
    return reduce(self.first) { selectedOffer, currentOffer in
      guard let selectedOffer = selectedOffer else { return currentOffer }
      
      if selectedOffer.apply(to: price) < currentOffer.apply(to: price) {
        return selectedOffer
      } else {
        return currentOffer
      }
    }
  }
}

struct OffersResponse: Decodable {
  let offers: [Offer]
}

struct ApplicableOffers: TransferableObject {
  typealias ResourceType = OffersResponse
  
  let booksReferences: [UUID]
  var getPath: String {
    let booksList = self.booksReferences.map { $0.uuidString.lowercased() }.joined(separator: ",")
    return "/books/\(booksList)/commercialOffers"
  }
}
