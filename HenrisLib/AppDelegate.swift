//
//  AppDelegate.swift
//  HenrisLib
//
//  Created by Jérôme Duquennoy on 13/07/2018.
//  Copyright © 2018 jduquennoy. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    // Override point for customization after application launch.
    return true
  }

}

