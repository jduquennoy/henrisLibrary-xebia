//
//  BookDetailsViewController.swift
//  HenrisLib
//
//  Created by Jérôme Duquennoy on 13/07/2018.
//  Copyright © 2018 jduquennoy. All rights reserved.
//

import UIKit

class BookDetailsViewController: UIViewController {
  @IBOutlet var coverView: UIImageView?
  @IBOutlet var titleLabel: UILabel?
  @IBOutlet var synopsisField: UITextView?

  override func viewWillAppear(_ animated: Bool) {
    self.navigationController?.isNavigationBarHidden = false
    self.navigationController?.navigationBar.topItem?.title = "Back to the store"
  }
  
  private func coverDidLoad(result: TransfertResult<UIImage>) {
    switch result {
    case .error:
      break;
    case .success(let image):
      DispatchQueue.main.async {
        self.coverView?.image = image
      }
    }
  }
}

extension BookDetailsViewController: BookConsumer {
  func load(book: Book?, with resourcesLoader: ResourcesLoader) {
    guard let book = book else {
      return
    }
    self.loadViewIfNeeded()
    let coverRequest = BookCover(book: book)
    resourcesLoader.get(coverRequest, callback: coverDidLoad)
    titleLabel?.text = book.title
    synopsisField?.text = book.synopsis.joined(separator: "\n")
    
  }
}
