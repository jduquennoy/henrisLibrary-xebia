//
//  ViewController.swift
//  HenrisLib
//
//  Created by Jérôme Duquennoy on 13/07/2018.
//  Copyright © 2018 jduquennoy. All rights reserved.
//

import UIKit

class BooksListViewController: UIViewController {
  let showBookDetailSegueName = "showBookDetails"

  @IBOutlet var booksTableView: UITableView?
  @IBOutlet var buyButton: UIButton?

  internal var libraryEndpoint: ResourcesLoader? = nil
  internal private(set) var books: [Book]? = nil {
    didSet {
      DispatchQueue.main.async {
        self.booksTableView?.reloadData()
      }
    }
  }
  private var cart = Set<Book>()

  override func awakeFromNib() {
    guard let serverUrl = URL(string: "http://henri-potier.xebia.fr") else {
      NSLog("Failed to initialize library endpoint: invalid URL")
      return
    }
    libraryEndpoint = RESTResourcesLoader(baseUrl: serverUrl)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.booksTableView?.separatorStyle = .none
  }
  
  override func viewWillAppear(_ animated: Bool) {
    self.navigationController?.isNavigationBarHidden = false
    self.navigationController?.navigationBar.topItem?.title = "Henri's library"
    self.loadBooks()
    self.updateBuyButton()
  }
  
  private func loadBooks() {
    guard let libraryEndpoint = self.libraryEndpoint else {
      self.booksDidFailToLoad(error: NSError.error(with: "Cannot load list of books for the moment."))
      return
    }
    
    libraryEndpoint.list(AllBooks()) { result in
      switch(result) {
      case .success(let books):
        self.booksDidLoad(books)
      case .error(let error):
        self.booksDidFailToLoad(error: error)
      }
    }
  }
  
  private func booksDidLoad(_ loadedBooks: [Book]) {
    self.books = loadedBooks
  }
  
  private func booksDidFailToLoad(error: Error) {
    let defaultAction = UIAlertAction(title: "Arg, too bad !",
                                      style: .default) { _ in }
    let alert = UIAlertController()
    alert.message = error.localizedDescription
    alert.addAction(defaultAction)
    self.present(alert, animated: true)
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    guard let libraryEndpoint = self.libraryEndpoint else { return }
    if let destination = segue.destination as? BookConsumer {
      destination.load(book: self.selectedBook(), with: libraryEndpoint)
    }
    if let destination = segue.destination as? BooksSetConsumer {
      destination.load(books: Array(self.cart), with: libraryEndpoint)
    }
  }
  
  private func selectedBook() -> Book? {
    guard let selectedIndex = self.booksTableView?.indexPathForSelectedRow?.last,
          let books = self.books,
          selectedIndex < books.count
    else {
      return nil
    }
    
    return books[selectedIndex]
  }
  
  @objc private func bookStatusDidChange(sender: UISwitch) {
    guard let book = self.books?[sender.tag] else { return }
    if sender.isOn {
      self.cart.insert(book)
    } else {
      self.cart.remove(book)
    }
    self.updateBuyButton()
  }
  
  private func updateBuyButton() {
    let selectedBooksCount = self.cart.count
    switch selectedBooksCount {
    case 0:
      self.buyButton?.isEnabled = false
      self.buyButton?.setTitle("Select books to buy", for: .disabled)
    case 1:
      self.buyButton?.isEnabled = true
      self.buyButton?.setTitle("Buy this book", for: .normal)
    default:
      self.buyButton?.isEnabled = true
      self.buyButton?.setTitle("Buy \(selectedBooksCount) books", for: .normal)
    }
  }
  
  @IBAction func buyButtonPressed() {
    self.performSegue(withIdentifier: "orderSelection", sender: self)
  }
}

extension BooksListViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.books?.count ?? 0
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let books = self.books, let bookIndex = indexPath.last else {
      return UITableViewCell()
    }
    let book = books[bookIndex]
    let cell = tableView.dequeueReusableCell(withIdentifier: "bookCell") as? BookTableCell ?? BookTableCell()
    cell.titleLabel?.text = book.title
    cell.priceLabel?.text = "\(book.price) €"
    cell.selectedSwitch?.isOn = self.cart.contains(book)
    cell.selectedSwitch?.tag = bookIndex
    cell.selectedSwitch?.addTarget(self, action: #selector(bookStatusDidChange(sender:)), for: .valueChanged)
    return cell
  }
}

extension BooksListViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    self.performSegue(withIdentifier: showBookDetailSegueName, sender: self)
    tableView.deselectRow(at: indexPath, animated: false)
  }
}


extension NSError {
  static func error(with message: String) -> Error {
    let infos = [NSLocalizedDescriptionKey: message]
    return NSError(domain: "BooksListViewController", code: 0, userInfo: infos)
  }
}
