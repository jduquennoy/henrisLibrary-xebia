//
//  BookTableCell.swift
//  HenrisLib
//
//  Created by Jérôme Duquennoy on 15/07/2018.
//  Copyright © 2018 jduquennoy. All rights reserved.
//

import UIKit

class BookTableCell: UITableViewCell {
  @IBOutlet var titleLabel: UILabel?
  @IBOutlet var priceLabel: UILabel?
  @IBOutlet var selectedSwitch : UISwitch?
}
